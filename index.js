#! /usr/bin/env node

var http = require('http');
var request = require('request');
const argv = require('yargs').argv
var fs = require('fs');

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

var proxyPort = argv.p ? argv.p : 3000;
var targetHost = argv.t ? argv.t : "https://www.google.com";
var headersFilePath = argv.h ? argv.h : "";

var headers = []

if (headersFilePath)
  headers = JSON.parse(fs.readFileSync(headersFilePath, 'utf8'));

http.createServer(function (req, res) {

  req.pipe(request({
    url: targetHost + req.url,
    qs: req.query,
    method: req.method,
    headers: headers,
    followRedirect: true,
    jar: true
  }))
    .on('request', function (req) {

    })
    .on('response', function (res) {

      res.headers = { ...res.headers, ...headers };

    })
    .pipe(res);

}).listen(proxyPort);

console.log("Listening at localhost:"+proxyPort);